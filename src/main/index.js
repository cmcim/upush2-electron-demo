'use strict'

import { app, BrowserWindow } from 'electron'
import '../renderer/store'
import { UPush, UPushListener } from 'upush2-electron-client'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 600,
    useContentSize: true,
    width: 800
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  initApp()
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

if (process.platform === 'win32') {
  app.on('window-all-closed', () => {
    UPush.unregisterUPush()
  })
} else {
  app.on('before-quit', () => {
    UPush.unregisterUPush()
  })
}

function initApp () {
  const upushListener = new UPushListener()
  upushListener.setOnPushToken((code, pushToken, userId) => {
    console.log('onPushToken', code, pushToken, userId)
    mainWindow.webContents.send('onPushToken', code, pushToken, userId)
  })

  upushListener.setOnStatus(status => {
    console.log('onStatus', status)
    if (mainWindow && !mainWindow.isDestroyed()) {
      mainWindow.webContents.send('onStatus', status)
    }
  })

  upushListener.setOnBindUser(status => {
    console.log('onBindUser', status)
    mainWindow.webContents.send('onBindUser', status)
  })

  // 使用async配合await可以使消息有序处理，但如果没有业务操作直接发给渲染进程，就像下面例子，
  // 可以不用写async。总之写不写都能正常使用，请开发者自行决定。
  upushListener.setOnPush(async (msg, timestamp, isOfflineMsg) => {
    console.log('onPush', JSON.stringify(msg), timestamp + '', isOfflineMsg)
    if (msg.notification) {
      // 使用msg.notification构建通知栏 https://www.electronjs.org/zh/docs/latest/api/notification
    }

    mainWindow.webContents.send('onPush', msg, timestamp, isOfflineMsg)
  })

  upushListener.setOnRevoke((data, timestamp) => {
    console.log('onRevoke', JSON.stringify(data), timestamp)
    mainWindow.webContents.send('onRevoke', data, timestamp)
  })

  upushListener.setOnUnregister(() => {
    console.log('onUnregister')
  })

  upushListener.setOnRegisteredOnAnotherDevice(info => {
    console.log('onRegisteredOnAnotherDevice', JSON.stringify(info))
  })

  UPush.configUPush('cn.com.upush.electron.sdk.app.test', 'd21085fc', '486f49ce6b21fbf57c9673a2a0e219a0', upushListener)
  // 这里设置为全局属性，只是为了直接在渲染进程中显示，实际开发没必要把地址设为全局属性
  global.upushUrl = 'ws://192.168.6.132:5488'
  UPush.registerUPush(global.upushUrl)

  // 如果App再次启动无需重复登录(例如微信被结束后再次启动无需输入用户密码也保持登录状态)，那么应用在检查完登录状态后，
  // 可以直接绑定登录用户ID，无需等待UPush注册成功
  // UPush.bindUser('xxxxxx')
}

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
